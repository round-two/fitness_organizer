from django.urls import path
from .views import home_detail, CalendarView


urlpatterns = [
    path("", home_detail, name="home"),
    path("calendar/", CalendarView.as_view(), name='calendar'),
]