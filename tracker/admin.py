from django.contrib import admin
from .models import UserProfile, Exercise, Meal
# Register your models here.
admin.site.register(UserProfile)
admin.site.register(Exercise)
admin.site.register(Meal)