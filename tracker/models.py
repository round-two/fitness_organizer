from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    height_in = models.PositiveIntegerField()
    weight_lb = models.FloatField()
    fitness_goal = models.TextField(blank=True)
    start_weight = models.FloatField()
    
    def height_in_feet(self):
            feet = self.height_in // 12
            inches = self.height_in % 12
            return f"{feet} feet {inches} inches"
    
    def __str__(self):
        return self.user
    
class Exercise(models.Model):
    name = models.CharField(max_length=100)
    sets = models.PositiveIntegerField(blank=True, null=True) 
    reps = models.PositiveIntegerField(blank=True, null=True)
    duration_minutes = models.PositiveIntegerField(blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    time = models.DateTimeField()

    def __str__(self):
        return self.name
    
class Meal(models.Model):
    name = models.CharField(max_length=100)
    food_items_and_weights = models.TextField()
    time = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

